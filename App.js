import React from "react";
import { Ionicons } from "@expo/vector-icons";
import Icon from 'react-native-vector-icons/Ionicons';

import { createAppContainer } from "react-navigation";
import { createMaterialBottomTabNavigator } from
    "react-navigation-material-bottom-tabs";

import Home from "./screens/Home";
import UserScreen from "./screens/UserScreen";
import Settings from "./screens/Settings";

const TabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: "Trang Chủ",
        tabBarIcon: (tabInfo) => (
          <Icon
            name="md-home"
            size={tabInfo.focused ? 24 : 20}
            color={tabInfo.tintColor}
          />
        ),
      },
    },
    User: {
      screen: UserScreen,
      navigationOptions: {
        tabBarLabel: "Cá nhân",
        tabBarIcon: (tabInfo) => (
          <Icon
            name="md-person-circle"
            size={tabInfo.focused ? 24 : 20}
            color={tabInfo.tintColor}
          />
        ),
      },
    },

  },
  {
    initialRouteName: "Home",
    barStyle: { backgroundColor: "#065c0b" },
  }
);

const Navigator = createAppContainer(TabNavigator);

export default function App() {
  return (
    <Navigator>
      <Home />
    </Navigator>
  );
}
