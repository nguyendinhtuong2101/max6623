import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Image,ImageBackground,Alert  } from 'react-native';

import logo from '../assets/logo.png';
import logo_main from '../assets/logo7.png';
import bg from '../assets/bg.jpg';

import { InAppBrowser } from 'react-native-inappbrowser-reborn'

class Home extends Component {

	constructor(props) {
		super(props);
		this.state = {
		links_reg: [
							{link_reg:''}
					],
		links_login: [
							{link_login:''}
					],
		links_support:[
				{support:''},
			]
			};
	}
   componentDidMount(){

		return fetch('https://webpower.store/max6623/result.php')
			.then((response) => response.json())
			.then((responseJson) => {
			//	console.log(responseJson[0]);
				this.setState({
				  link_reg:responseJson[0]['registry'],
					link_login:responseJson[0]['login'],
					support:responseJson[0]['support'],
					//title:'abc'

				}, function(){

				});

			})
			.catch((error) =>{
				console.error(error);
			});
	}

	handleButtonPress(link) {
		this.props.navigation.navigate('Welcome', {link })
	}

	render() {
		const openLink = async (url) => {
				 try {
					 const isAvailable = await InAppBrowser.isAvailable()
					 if (isAvailable) {
						 InAppBrowser.open(url, {
							 // Android Properties
							 showTitle: false,
							 toolbarColor: '#fff',
							 secondaryToolbarColor: '#fff',
							 enableUrlBarHiding: true,
							 enableDefaultShare: false,
							 forceCloseOnRedirection: true,
							 navigationBarColor: '#fff',
							 navigationBarDividerColor: '#fff',

						 }).then((result) => {
							 console.log(JSON.stringify(result))
						 })
					 } else Linking.openURL(url)
				 } catch (error) {
					 console.log(error.message)
				 }
			 }

		const handleButtonDN = (linkafter) => {
					return Alert.alert(
						"",
						"Bạn có muốn tiếp tục truy cập ?",
						[
							{
								text: "Có",
								onPress: () => {
								openLink(linkafter)
								},
							},

							{
								text: "Không",
							},
						]
					);
				};
	const day = new Date().getTimezoneOffset() / -60;

		return (

			<View style={styles.container}>
			    <View style={{flex: 1}}>
			      <View  style={styles.header}>
							<ImageBackground resizeMode='cover'  source={bg} style={styles.header_bg}>
										<View  style={styles.logo_t}>

											     <Image source={logo}
													style={{width: 200, height:200, borderRadius:10}} />

													{ day === 7 ?
														<View style={styles.backgroup1} >
								               {this.state.links_reg.map((item, index) => (
								                    <TouchableOpacity
								                      key={index}
								                      onPress={() => handleButtonDN(this.state.link_reg)}
								                    >
								                      <Text style={styles.container_button}>ĐĂNG KÝ</Text>
								                    </TouchableOpacity>
								                  ))}
								            </View>
														: null}
														{ day === 7 ?
														<View style={styles.backgroup2} >
																 {this.state.links_reg.map((item, index) => (
																			<TouchableOpacity
																				key={index}
																				onPress={() => handleButtonDN(this.state.link_login)}
																			>
																				<Text style={styles.container_button}>ĐĂNG NHẬP</Text>
																			</TouchableOpacity>
																		))}
															</View>
															: null}
								             <View style={styles.backgroup3}>
								             {this.state.links_support.map((item, index) => (
								                  <TouchableOpacity
								                    key={index}
								                    onPress={() => handleButtonDN(this.state.support)}
								                  >
								                    <Text style={styles.button3}>HỖ TRỢ</Text>
								                  </TouchableOpacity>
								                ))}
								              </View>
										</View>
							</ImageBackground>
			      </View>
			    </View>
			</View>

		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header:{
			 flex: 2,
			 justifyContent: 'center',
			 alignItems: 'center',
			 backgroundColor: '#fff',
			 marginTop:-30,
	},
	header_bg:{
		width: '100%',
			 height: '100%',
			 flex: 1

	},
	logo_t:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		//marginBottom:20,
	},

	backgroup1: {
			marginTop:20,
			backgroundColor:'#c14d26',
			width:200,
			height:50,
			alignItems: 'center',
			borderRadius: 20,
			borderWidth: 1,
			flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
			borderColor:'#fff'
	},
	backgroup2: {
			marginTop:20,
			backgroundColor:'#065c0b',
			width:200,
			height:50,
			alignItems: 'center',
			borderRadius: 20,
			flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
			borderWidth: 1,
			borderColor:'#FFF'
	},
	backgroup3: {
			marginTop:20,
			backgroundColor:'#170240',
			width:200,
			height:50,
			alignItems: 'center',
			borderRadius: 20,
			borderWidth: 1,
			flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
			borderColor:'#FFF'
	},
		container_button: {
			 color: '#FFF',
			 fontWeight: "bold",
			 fontSize:20,
		 },
		 button3: {
				alignItems: 'center',
				justifyContent: 'center',
				textAlign:'center',
				paddingTop: 5,
				color: '#FFF',
				fontWeight: "bold",
				fontSize:20,

			},
			text_heder: {
				 alignItems: 'center',
				 justifyContent: 'center',
				 textAlign:'center',
				 paddingTop:20,
				 color: '#FE642E',
				 fontWeight: "bold",
				 fontSize:24,
				 textShadowOffset: {width: 2, height: 2},
				 textShadowRadius: 10,
				 textShadowColor: '#000',
			 },
			 text_heder2: {
 				 alignItems: 'center',
 				 justifyContent: 'center',
 				 textAlign:'center',
 				// paddingTop:25,
 				 color: '#FE642E',
 				 fontWeight: "bold",
 				 fontSize:25,

		    textShadowOffset: {width: 2, height: 2},
		    textShadowRadius: 10,
		    textShadowColor: '#000',
 			 },
})

export default Home
